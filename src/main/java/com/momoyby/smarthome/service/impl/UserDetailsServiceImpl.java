package com.momoyby.smarthome.service.impl;

import com.momoyby.smarthome.entity.User;
import com.momoyby.smarthome.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author 于本洋.
 * @date 2022/2/19.
 * @time 18:24.
 * @email 736888980@qq.com
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserMapper adminMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userDetail = adminMapper.loadUserByUsername(username);
        if (null == userDetail) {
            throw new UsernameNotFoundException("用户名不存在!");
        }
        // admin.setRoles(adminMapper.getHrRolesById(admin.getId()));
        return userDetail;
    }
}
