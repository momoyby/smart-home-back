package com.momoyby.smarthome.controller;

import com.momoyby.smarthome.entity.RespBean;
import com.momoyby.smarthome.service.impl.UserDetailsServiceImpl;
import com.momoyby.smarthome.util.VerificationCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * @author 于本洋.
 * @date 2021/11/27.
 * @time 18:30.
 * @email 736888980@qq.com
 */
@Api(value = "LoginController", tags = {"登录接口"})
@RestController
public class LoginController {

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @ApiOperation("获取验证码")
    @GetMapping("/verifyCode")
    public void verifyCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        VerificationCode code = new VerificationCode();
        BufferedImage image = code.getImage();
        String text = code.getText();
        HttpSession session = request.getSession(true);
        System.out.println(text);
        session.setAttribute("verify_code", text);
        VerificationCode.output(image,response.getOutputStream());
    }

    @GetMapping("/logout")
    public RespBean logout(){
        return RespBean.ok("注销成功");
    }

}
