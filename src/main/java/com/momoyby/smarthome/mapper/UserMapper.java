package com.momoyby.smarthome.mapper;

import com.momoyby.smarthome.entity.User;
import org.springframework.stereotype.Component;

/**
 * @author 于本洋.
 * @date 2022/2/19.
 * @time 18:26.
 * @email 736888980@qq.com
 */
@Component
public interface UserMapper {

    User loadUserByUsername(String username);
}
