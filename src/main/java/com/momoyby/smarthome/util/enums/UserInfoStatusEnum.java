package com.momoyby.smarthome.util.enums;

import java.util.stream.Stream;

/**
 * @author 于本洋.
 * @date 2022/2/6.
 * @time 18:03.
 * @email 736888980@qq.com
 */
public enum UserInfoStatusEnum {

    //用户 帐号状态（0、正常；1、禁用）
    NORMAL("0", "正常"),
    DISABLE("1", "禁用"),
            ;

    public final String code;
    public final String name;

    UserInfoStatusEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static UserInfoStatusEnum getByCode(String code) {
        return Stream.of(UserInfoStatusEnum.values())
                .filter(userInfoStatusEnum -> userInfoStatusEnum.code.equals(code))
                .findAny()
                .orElse(null);
    }
}
