package com.momoyby.smarthome.entity;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 于本洋.
 * @date 2021/12/18.
 * @time 21:13.
 * @email 736888980@qq.com
 */
@ApiModel(value = "RespBean", description = "响应实体")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RespBean {

    private Integer status;
    private String msg;
    private Object data;

    public static RespBean ok(String msg) {
        return new RespBean(200, msg, null);
    }

    public static RespBean ok(String msg, Object data) {
        return new RespBean(200, msg, data);
    }

    public static RespBean error(String msg) {
        return new RespBean(500, msg, null);
    }

    public static RespBean error(String msg, Object data) {
        return new RespBean(500, msg, data);
    }

}
