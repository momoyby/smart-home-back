package com.momoyby.smarthome.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * @author 于本洋.
 * @date 2022/2/19.
 * @time 17:06.
 * @email 736888980@qq.com
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiinfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.momoyby.smarthome"))
                .build();
    }

    private ApiInfo apiinfo(){
        final Contact DEFAULT_CONTACT = new Contact("YBY_智慧家居项目组", "https://gitee.com/momoyby", "736888980@qq.com");
        return new ApiInfo(
                "Api 智慧家居文档",
                "Api Documentation",
                "1.0",
                "http://localhost:8081",
                DEFAULT_CONTACT,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<>());
    }
}
